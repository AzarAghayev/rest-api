angular.module("product",["ngRoute", 'ngAnimate'])
.directive("fileModel", ["$parse", function($parse) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            let parsedFile = $parse(attrs.ngModel);
            let parsedFileSetter = parsedFile.assign;

            element.bind('change', function() {
                scope.$apply(function() {
                    parsedFileSetter(scope, element[0].files[0]);
                });
            });
        }
    }
}])
.directive('stringToNumber', function() {
    return {
        require: 'ngModel',
        link: function(scope, element, attrs, ngModel) {
            ngModel.$parsers.push(function(value) {
                return '' + value;
            });
            ngModel.$formatters.push(function(value) {
                return parseFloat(value);
            });
        }
    };
})
.controller("ProductController", ["$scope", "ProductService", "GeneralService", "$timeout", "$location", "$routeParams", function($scope, ProductService, GeneralService, $timeout, $location, $routeParams){
    
    $scope.getProducts = function(){
        ProductService.getProducts().then(function onSuccess(response) {
            $scope.products = response.data.products;
            $scope.array = response.data.categories;
            let noparent = [];
            let parent = [];
            $scope.array.forEach(element => {
                if (!element.parent) {
                    noparent.push(element);
                }
                else {
                    parent.push(element);
                }
            });
            $scope.categories = parent;
            $scope.markas = response.data.markas;
        },function onError(err){
            console.log(err);
        });
    };

    $scope.getProduct = function(){
        let _id = $routeParams._id;
        ProductService.getProduct(_id).then(function onSuccess(response) {
            $scope.product = response.data.product;
            console.log($scope.product);
            $scope.array = response.data.categories;
            let noparent = [];
            let parent = [];
            $scope.array.forEach(element => {
                if (!element.parent) {
                    noparent.push(element);
                }
                else {
                    parent.push(element);
                }
            });
            $scope.categories = parent;
            $scope.markas = response.data.markas;
        })
    };

    $scope.addProduct = function(){
        const fd = new FormData();
        fd.append('image', $scope.product.image);
        fd.append('name', $scope.product.name);
        fd.append('model', $scope.product.model);
        fd.append('marka', $scope.product.marka);
        fd.append('price', $scope.product.price);
        fd.append('category', $scope.product.category);
        fd.append('description', $scope.product.description);
        $scope.uploading = true;

        ProductService.addProduct(fd).then(function onSuccess(response){
            console.log(response.data);
            $location.path("/products");
        },function onError(err){
          console.log(err);
        });
    };

    $scope.updateProduct = function(){
        let _id = $routeParams._id;
        const fd = new FormData();
        fd.append('image', $scope.product.image);
        fd.append('name', $scope.product.name);
        fd.append('model', $scope.product.model);
        fd.append('marka', $scope.product.marka);
        fd.append('price', $scope.product.price);
        fd.append('category', $scope.product.category);
        fd.append('description', $scope.product.description);
        ProductService.updateProduct(_id, fd).then(function onSuccess(response){
            console.log(response.data);
            $location.path("/products");
        },function onError(err){
          console.log(err);
        });
    };

    $scope.deleteProduct = function(_id){
        ProductService.deleteProduct(_id).then(function onSuccess(response) {
            console.log(response.data);
            $location.path("/products");
        },function onError(err) {
            console.log(err);
        });
    };
}]);