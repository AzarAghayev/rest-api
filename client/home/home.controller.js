angular.module("home",["ngRoute", "angular.filter"])
.controller("HomeController", ["$scope", "GeneralService", "$filter", function($scope, GeneralService, $filter){
    $scope.Navbar = function(){
        GeneralService.getGeneral().then(function onSuccess(response){
            $scope.array = response.data.categories;
            let noparent = [];
            let parent = [];
            $scope.array.forEach(element => {
                if (!element.parent) {
                    noparent.push(element);
                }
                else {
                    parent.push(element);
                }
            });
            $scope.navs = noparent;
            $scope.categories = parent;
        },function onError(err){
          console.log(err);
        })

    };
}])