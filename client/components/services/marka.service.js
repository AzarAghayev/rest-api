myApp.factory("MarkaService", ["$http", function($http) {
    let getMarkas = function () {
        return $http.get("api/markas");
    };

    let getMarka = function (_id) {
        return $http.get("api/markas/" + _id);
    };

    let addMarka = function (marka) {
        return $http.post("api/markas/", marka);
    };

    let updateMarka = function(_id, marka) {
        return $http.put("api/markas/" + _id, marka);
    };

    let deleteMarka = function(_id) {
        return $http.delete("api/markas/" + _id);
    };

    return {
        getMarkas: getMarkas,
        getMarka: getMarka,
        addMarka: addMarka,
        updateMarka: updateMarka,
        deleteMarka: deleteMarka
    }
}]);