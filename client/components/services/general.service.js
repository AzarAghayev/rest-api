myApp.factory("GeneralService", ["$http", "$window", function($http, $window) {
    let getGeneral = function() {
        return $http.get("api");
    };

    let setCategoriesLocal = function(categories) {
        if(categories) {
            $window.localStorage.setItem("categories", JSON.stringify(categories));
        } 
        else {
            $window.localStorage.removeItem("categories");
        }
    };

    let getCategoriesLocal = function() {
        return JSON.parse($window.localStorage.getItem("categories"));
    };

    let setMarkasLocal = function(markas) {
        if(markas) {
            $window.localStorage.setItem("markas", JSON.stringify(markas));
        } 
        else {
            $window.localStorage.removeItem("markas");
        }
    };

    let getMarkasLocal = function() {
        return JSON.parse($window.localStorage.getItem("markas"));
    };

    let setProductsLocal = function(products) {
        if (products) {
            $window.localStorage.setItem("products", JSON.stringify(products));
        } 
        else {
            $window.localStorage.removeItem("products");
        }
    };

    let getProductsLocal = function() {
        return JSON.parse($window.localStorage.getItem("products"));
    };

    let clearLocal = function(){
        $window.localStorage.removeItem("categories");
        $window.localStorage.removeItem("markas");
        $window.localStorage.removeItem("products");
        return;
    }

    return {
        getGeneral: getGeneral,
        setCategoriesLocal: setCategoriesLocal,
        getCategoriesLocal: getCategoriesLocal,
        setMarkasLocal: setMarkasLocal,
        getMarkasLocal: getMarkasLocal,
        setProductsLocal: setProductsLocal,
        getProductsLocal: getProductsLocal,
        clearLocal: clearLocal
    }
}]);