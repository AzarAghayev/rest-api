myApp.factory("ProductService", ["$http", function($http) {
    let getProducts = function() {
        return $http.get("api/products");
    };

    let getProduct = function(_id) {
        return $http.get("api/products/" + _id);
    };

    let addProduct = function (all) {
        return $http.post("api/products", all, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        });
    };

    let updateProduct  = function(_id, product) {
        return $http.put("api/products/" + _id, product, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        });
    };

    let deleteProduct = function(_id) {
        return $http.delete("api/products/" + _id);
    };

    return {
        getProducts: getProducts,
        getProduct: getProduct,
        addProduct: addProduct,
        updateProduct: updateProduct,
        deleteProduct: deleteProduct
    }
}]);