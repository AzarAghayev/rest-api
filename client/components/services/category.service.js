myApp.factory("CategoryService", ["$http", "$window", function($http, $window) {
    let getCategories = function () {
        return $http.get("api/categories/");
    };

    let getCategory = function (_id) {
        return $http.get("api/categories/" + _id);
    };

    let addCategory = function (category) {
        return $http.post("api/categories/", category);
    };

    let updateCategory = function(_id, category) {
        return $http.put("api/categories/" + _id, category);
    };

    let deleteCategory = function(_id) {
        return $http.delete("api/categories/" + _id);
    };

    return {
        getCategories: getCategories,
        getCategory: getCategory,
        addCategory: addCategory,
        updateCategory: updateCategory,
        deleteCategory: deleteCategory
    }
}]);