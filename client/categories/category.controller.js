angular.module("category",["ngRoute"])
.controller("CategoryController", ["$scope", "CategoryService", "GeneralService", "$location", "$routeParams", function($scope, CategoryService, GeneralService, $location, $routeParams){
    $scope.getCategories = function() {
        CategoryService.getCategories().then( function onSuccess(response) {
            $scope.categories = response.data.categories;
        },function onError(err){
            console.log(err);
        });
    };

    $scope.getCategory = function() {
        let _id = $routeParams._id;
        CategoryService.getCategories().then( function onSuccess(response) {
            $scope.categories = response.data.categories;
            $scope.categories.forEach(element => {
                if (element._id == _id) {
                    $scope.clickedElement = element;
                }
            });
        },function onError(err){
            console.log(err);
        });
    };

    $scope.addCategory = function(){
        CategoryService.addCategory($scope.category).then(function onSuccess(response){
            console.log(response.data);
            $location.path("/categories");
        },function onError(err){
          console.log(err);
        });
    };

    $scope.updateCategory = function(){
        console.log($scope.clickedElement);
        let _id = $routeParams._id;
        CategoryService.updateCategory(_id, $scope.clickedElement).then(function onSuccess(response){
            console.log(response.data);
            $location.path("/categories");
        },function onError(err){
          console.log(err);
        });
    };

    $scope.deleteCategory = function(_id){
        CategoryService.deleteCategory(_id).then(function onSuccess(response) {
            console.log(response.data);
            $location.path("/categories");
        },function onError(err) {
            console.log(err);
        });
    };
}]);