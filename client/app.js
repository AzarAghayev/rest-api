var myApp = angular.module("myApp", ["ngRoute", "home", "category", "marka", "product"]);

myApp.config(function ($routeProvider, $locationProvider) {
    $locationProvider.html5Mode(true);
    $routeProvider.when("/", {
            controller: "HomeController",
            templateUrl: "home/home.html"
        })
        .when("/categories", {
            controller: "CategoryController",
            templateUrl: "categories/category.html"
        })
        .when("/categories/add", {
            controller: "CategoryController",
            templateUrl: "categories/add_category.html"
        })
        .when("/categories/edit/:_id", {
            controller: "CategoryController",
            templateUrl: "categories/edit_category.html"
        })
        .when("/markas", {
            controller: "MarkaController",
            templateUrl: "markas/marka.html"
        })
        .when("/markas/add", {
            controller: "MarkaController",
            templateUrl: "markas/add_marka.html"
        })
        .when("/markas/edit/:_id", {
            controller: "MarkaController",
            templateUrl: "markas/edit_marka.html"
        })
        .when("/products", {
            controller: "ProductController",
            templateUrl: "products/product.html"
        })
        .when("/products/details/:_id", {
            controller: "ProductController",
            templateUrl: "products/product_details.html"
        })
        .when("/products/add", {
            controller: "ProductController",
            templateUrl: "products/add_product.html"
        })
        .when("/products/edit/:_id", {
            controller: "ProductController",
            templateUrl: "products/edit_product.html"
        })
        .otherwise({
            redirectTo: "/",
        })
});