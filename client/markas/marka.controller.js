angular.module("marka",["ngRoute"])
.controller("MarkaController", ["$scope", "MarkaService", "GeneralService", "$location", "$routeParams", function($scope, MarkaService, GeneralService, $location, $routeParams){
    $scope.getMarkas = function() {
        MarkaService.getMarkas().then(function onSuccess(response) {
            $scope.markas = response.data.markas;
        },function onError(err){
            console.log(err);
        });
    };

    $scope.getMarka = function(){
        let _id = $routeParams._id;
        MarkaService.getMarka(_id).then(function onSuccess(response) {
            $scope.marka = response.data.marka;
        },function onError(err){
            console.log(err);
        });
    };

    $scope.addMarka = function(){
        MarkaService.addMarka($scope.marka).then(function onSuccess(response){
            console.log(response.data);
            $location.path("/markas");
        },function onError(err){
          console.log(err);
        });
    };

    $scope.updateMarka = function(){
        let _id = $routeParams._id;
        MarkaService.updateMarka(_id, $scope.marka).then(function onSuccess(response){
            console.log(response.data);
            $location.path("/markas");
        },function onError(err){
          console.log(err);
        });
    };

    $scope.deleteMarka = function(_id){
        MarkaService.deleteMarka(_id).then(function onSuccess(response) {
            console.log(response.data);
            $location.path("/markas");
        },function onError(err) {
            console.log(err);
        });
    };
}]);