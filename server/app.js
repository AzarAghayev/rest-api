const express = require("express");
const mongoose = require("mongoose");
const keys = require("./config/keys");
const logger = require("morgan");
const bodyParser = require("body-parser");
const path = require("path");

let app = express();

//Router
let generalRoute = require('./api/general/general.route');
let categoryRoute = require('./api/products/categories/category.route');
let markaRoute = require('./api/products/markas/marka.route');
let productRoute = require('./api/products/product/product.route');

//mongoose 
mongoose.Promise = global.Promise;
mongoose.connect(keys.database, keys.options);

//Middlewares
app.use(logger('dev'));
app.use(express.static(__dirname + "/../client"));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:false}));

//routes in server
app.use("/api", generalRoute);
app.use("/api/categories", categoryRoute);
app.use("/api/markas", markaRoute);
app.use("/api/products", productRoute);

app.get('*', (req, res)=> {
    res.sendFile(path.join(__dirname, '/../client/index.html')); // load the single view file (angular will handle the page changes on the front-end)
});

app.listen(3000, () => {
    console.log("Server runs on localhost with the port no:3000");
})