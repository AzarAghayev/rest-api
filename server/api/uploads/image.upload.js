const multer = require("multer");
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, './client/uploads/images/')
    },
    filename: function (req, file, cb) {
        if (!file.originalname.match(/\.(jpeg|jpg|png)$/)) {
            const err = new Error();
            err.code = 'filetype';
            return cb(err);
        }
        else {
            cb(null, Date.now() + '-' + file.originalname);
        }
    }
})

const upload = multer({ 
    storage: storage, 
    limits: {fileSize: 10000000} 
}).single('image');

module.exports = {storage, upload}