const Category = require('../products/categories/category.model');

//Get markas and categories
function getCategories(req, res) {
    Category.find().exec((err, categories) => {
        if (err) {
            res.json({status:201, message: "Failed operation"});
        } else {
            res.json({status:200, message: "Success operation", categories: categories});
        }
    });
}

module.exports = {getCategories}