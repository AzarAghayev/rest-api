const router = require("express").Router();
const generalController = require('./general.controller');

//Get markas and categories
router.get("/", (req, res) => {
    generalController.getCategories(req, res);
})

module.exports = router;