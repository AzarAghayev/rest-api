const Marka = require('./marka.model');

//Get markas
function getMarkas(req, res) {
    Marka.find().exec((err, markas) => {
        if (err) {
            res.status(404);
            res.json({message: "Failed operation"})
        }
        else {
            res.status(200);
            res.json({message: "Success operation", markas: markas});
        }
    })
}

//Get marka
function getMarka(req, res) {
    const _id = req.params._id;
    Marka.findById(_id).exec((err, marka) => {
        if (err) {
            res.status(404);
            res.json({message: "Failed operation"})
        }
        else {
            res.status(200);
            res.json({message: "Success operation", marka: marka});
        }
    })
}

//Create marka
function addMarka(req, res) {
    let name = req.body.name;
    let newMarka = new Marka({
        name: name
    });
    newMarka.save((err, marka) => {
        if (err) {
            res.status(404);
            res.json({message: "Failed operation"})
        }
        else {
            res.status(201);
            res.json({message: "Success operation", marka: marka});
        }
    });
}

//Update marka
function updateMarka(req, res) {
    let query = {_id: req.params._id};
    let update = {
        name: req.body.name
    };
    Marka.findOneAndUpdate(query, update).exec((err, result) => {
        if (err) {
            res.status(404);
            res.json({message: "Failed operation"});
        }
        else {
            res.status(200);
            res.json({message: "Success operation", marka: result});
        }
    });
}

//Delete marka
function deleteMarka(req, res) {
    let query = {_id: req.params._id};
    Marka.remove(query).exec((err, result) => {
        if (err) {
            res.status(404);
            res.json({message: "Failed operation"});
        }
        else {
            res.status(200);
            res.json({message: "Success operation", result: result});
        }
    });
}

module.exports = {getMarkas, getMarka, addMarka, updateMarka, deleteMarka};