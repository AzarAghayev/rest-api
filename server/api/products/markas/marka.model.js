const mongoose = require("mongoose");

let markaShema = mongoose.Schema({
    name: {type: String, required: true}
})

let Marka = module.exports = mongoose.model("Marka", markaShema);