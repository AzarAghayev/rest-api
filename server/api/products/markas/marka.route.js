const router = require("express").Router();
const markaController = require('./marka.controller');

//Get markas
router.get("/", (req, res) => {
    markaController.getMarkas(req, res);
});

//Get marka
router.get("/:_id", (req, res) => {
    markaController.getMarka(req, res);
});

// Add marka
router.post("/", (req, res) => {
    markaController.addMarka(req, res);
});

//Update marka
router.put("/:_id", (req, res) => {
    markaController.updateMarka(req, res);
});

//Delete marka
router.delete("/:_id", (req, res) => {
    markaController.deleteMarka(req, res);
})

module.exports = router;