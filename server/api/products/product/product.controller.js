const Product = require('./product.model');
const Category = require('../categories/category.model');
const Marka = require('../markas/marka.model');
const fs = require('fs');
const path = require('path');

//Get Products
function getProducts(req, res) {
    let dbcategories = {};
    let dbmarkas = {};
    Category.find().exec((err, result) => {
        if (err) {
            res.status(404);
            res.json({message: "Failed operation"});
        }
        else {
            dbcategories = result;
            Marka.find().exec((err, result) => {
                if (err) {
                    res.status(404);
                    res.json({message: "Failed operation"});
                }
                else {
                    dbmarkas = result;
                    Product.find().populate('marka').populate('category').exec((err, products) => {
                        if (err) {
                            res.status(404);
                            res.json({message: "Failed operation"});
                        }
                        else {
                            res.status(200);
                            res.json({message: "Success operation", products: products, categories: dbcategories, markas: dbmarkas});
                        }
                    });
                }
            });
        }
    });
};

//Get Product by Id
function getProduct(req, res) {
    let _id = req.params._id;
    let dbcategories = {};
    let dbmarkas = {};
    Category.find().exec((err, result) => {
        if (err) {
            res.status(404);
            res.json({message: "Failed operation"});
        }
        else {
            dbcategories = result;
            Marka.find().exec((err, result) => {
                if (err) {
                    res.status(404);
                    res.json({message: "Failed operation"});
                }
                else {
                    dbmarkas = result;
                    Product.findById(_id).populate('marka').populate('category').exec((err, product) => {
                        if (err) {
                            res.status(404);
                            res.json({message: "Failed operation"});
                        }
                        else {
                            res.status(200);
                            res.json({message: "Success operation", product: product, categories: dbcategories, markas: dbmarkas});
                        }
                    });
                }
            });
        }
    });
};

//Add Product
function addProduct(req, res) {
    let newProduct = {};
    if (req.file) {
        newProduct = new Product({
            name: req.body.name,
            model: req.body.model,
            marka: req.body.marka,
            price: req.body.price,
            category: req.body.category,
            description: req.body.description,
            image: req.file.filename
        });
    } else {
        newProduct = new Product({
            name: req.body.name,
            model: req.body.model,
            marka: req.body.marka,
            price: req.body.price,
            category: req.body.category,
            description: req.body.description
        });
    }
    newProduct.save((err, product) => {
        if (err) {
            res.status(404);
            res.json({message: "Failed operation"})
        }
        else {
            res.status(201);
            res.json({message: "Success operation", product: product});
        }
    });
}

//Update product
function updateProduct(req, res) {
    let query = {_id: req.params._id};
    let update = {};
    if (req.file) {
        update = {
            name: req.body.name,
            model: req.body.model,
            marka: req.body.marka,
            price: req.body.price,
            category: req.body.category,
            description: req.body.description,
            image: req.file.filename
        };
    }
    else {
        update = {
            name: req.body.name,
            model: req.body.model,
            marka: req.body.marka,
            price: req.body.price,
            category: req.body.category,
            image: req.body.image,
            description: req.body.description
        };
    }
    Product.findOneAndUpdate(query,update).exec((err, result) => {
        if (err) {
            res.status(404);
            res.json({message: "Failed operation"});
        }
        else {
            res.status(200);
            res.json({message: "Success operation", product: result});
        }
    });
}

//Delete product
function deleteProduct(req, res) {
    const _id = {_id: req.params._id};
    Product.findById(_id).exec((err, product) => {
        if (err) {
            res.status(404);
            res.json({message: "Failed operation"});
        }
        else {
            if (product.image) {
                console.log('sekil var');
                console.log(product.image);
                fs.unlinkSync(path.join(__dirname, '/../../../../client/uploads/images/'+product.image), (err) => {
                    if (err) throw err;
                    console.log(req.params.image + ' was deleted');
                });
                Product.deleteOne(_id).exec((err, result) => {
                    if (err) {
                        res.status(404);
                        res.json({message: "Failed operation"});
                    }
                    else {
                        res.status(200);
                        res.json({message: "Success operation", result: result});
                    }
                });
            }
            else {
                Product.deleteOne(_id).exec((err, result) => {
                    if (err) {
                        res.status(404);
                        res.json({message: "Failed operation"});
                    }
                    else {
                        res.status(200);
                        res.json({message: "Success operation", result: result});
                    }
                });
            }
        }
    });
    
}

module.exports = {getProducts, getProduct, addProduct, updateProduct, deleteProduct};