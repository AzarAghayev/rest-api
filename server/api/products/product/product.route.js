const router = require("express").Router();
const productController = require('./product.controller');
const imageUpload = require("../../uploads/image.upload");

//Get products
router.get("/", (req, res) => {
    productController.getProducts(req, res);
})

//Get product
router.get("/:_id", (req, res) => {
    productController.getProduct(req, res);
})

//add product
router.post("/", (req, res) => {
    imageUpload.upload(req, res, function (err) {
        if (err) {
            if (err.code === 'LIMIT_FILE_SIZE') {
                res.json({success: false, message: 'File size is too larege. Max limit is 10MB'});
            }
            else if (err.code === 'filetype') {
                res.json({success: false, message: 'File type is invalid. Must be .jpeg .jpg .png'});
            }
            else {
                
                res.json({success: false, message: 'File was not able to be uploaded'});
            }
        } 
        else {
            /* if (!req.file) {
                res.json({success: false, message: 'No file was selected'});
            }
            else {
                productController.addProduct(req, res);
            } */
            productController.addProduct(req, res);
        }
    })
})

//edit product
router.put("/:_id", (req, res) => {
    imageUpload.upload(req, res, function (err) {
        if (err) {
            if (err.code === 'LIMIT_FILE_SIZE') {
                res.json({success: false, message: 'File size is too larege. Max limit is 10MB'});
            }
            else if (err.code === 'filetype') {
                res.json({success: false, message: 'File type is invalid. Must be .jpeg .jpg .png'});
            }
            else {
                
                res.json({success: false, message: 'File was not able to be uploaded'});
            }
        } 
        else {
            /* if (!req.file) {
                res.json({success: false, message: 'No file was selected'});
            }
            else {
                productController.updateProduct(req, res);
            } */
            productController.updateProduct(req, res);
        }
    })
})

//delete product
router.delete("/:_id", (req, res) => {
    productController.deleteProduct(req, res);
})

module.exports = router;