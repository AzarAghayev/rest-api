const mongoose = require("mongoose");
const Schema = mongoose.Schema;
let productShema = mongoose.Schema({
    name: {type: String, required: true},
    model: {type: String},
    marka: {type: Schema.Types.ObjectId, ref: 'Marka', required: true},
    price: {type: String, required: true},
    category: {type: Schema.Types.ObjectId, ref: 'Category', required: true},
    image: {type: String},
    description : {type: String, required: true}
})

let Product = module.exports = mongoose.model("Product", productShema);