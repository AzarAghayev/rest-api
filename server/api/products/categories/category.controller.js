const Category = require('./category.model');

//Get Categories
function getCategories(req, res) {
    Category.find().exec((err, categories) => {
        if (err) {
            res.status(404);
            res.json({message: "Failed operation"});
        }
        else {
            res.status(200);
            res.json({message: "Success operation", categories: categories});
        }
    })
}

//Get Category
function getCategory(req, res) {
    const _id = req.params._id;
    Category.findById(_id).exec((err, category) => {
        if (err) {
            res.status(404);
            res.json({message: "Failed operation"});
        }
        else {
            res.status(200);
            res.json({message: "Success operation", category: category});
        }
    })
}

//Create Category
function addCategory(req, res) {
    let name = req.body.name;
    let parent = req.body.parent;
    let newCategory = new Category({
        name: name,
        parent: parent
    });
    newCategory.save((err, category) => {
        if (err) {
            res.status(404);
            res.json({message: "Failed operation"})
        }
        else {
            res.status(201);
            res.json({message: "Success operation", category: category});
        }
    });
}

//Update Category
function updateCategory(req, res) {
    let query = {_id: req.params._id};
    let update = {
        name: req.body.name,
        parent: req.body.parent
    };
    Category.findOneAndUpdate(query, update).exec((err, result) => {
        if (err) {
            res.status(404);
            res.json({message: "Failed operation"});
        }
        else {
            res.status(200);
            res.json({message: "Success operation", category: result});
        }
    });
}

//Delete Category
function deleteCategory(req, res) {
    let query = {_id: req.params._id};
    Category.remove(query).exec((err, result) => {
        if (err) {
            res.status(404);
            res.json({message: "Failed operation"});
        }
        else {
            res.status(200);
            res.json({message: "Success operation", result: result});
        }
    });
}

module.exports = {getCategories, getCategory, addCategory, updateCategory, deleteCategory};