const router = require('express').Router();
const CategoryController = require('./category.controller');

//Get categories
router.get("/", (req, res) => {
    CategoryController.getCategories(req, res);
});

//Get category
router.get("/:_id", (req, res) => {
    CategoryController.getCategory(req, res);
});

// Add category
router.post("/", (req, res) => {
    CategoryController.addCategory(req, res);
});

//Update category
router.put("/:_id", (req, res) => {
    CategoryController.updateCategory(req, res);
});

//Delete category
router.delete("/:_id", (req, res) => {
    CategoryController.deleteCategory(req, res);
})

module.exports = router;