const mongoose = require("mongoose");

let categoryShema = mongoose.Schema({
    name: {type: String, required: true},
    parent: {type: String}
});

let Category = module.exports = mongoose.model("Category", categoryShema);